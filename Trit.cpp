#include "Trit.h"
#include <stdexcept>

using namespace trl;

trl::Trit::Trit():_state(TritState::O)
{}

trl::Trit::Trit(bool arg): _state(arg?TritState::I:TritState::T) //??? Will it work?
{}

trl::Trit::Trit(TritState arg): _state(arg)
{}

trl::Trit::Trit (char arg)
{
switch(arg)
{
	case 'T':_state=TritState::T;break;
	case 'O':_state=TritState::O;break;
	case 'I':_state=TritState::I;break;
	default: throw std::runtime_error("No conversion to trit for this char");


}
}
/*Trit & operator=(bool arg)
{
_state= (arg? I : T);
return *this;
}*/

Trit Trit::operator& (const Trit& arg) const
{
return Trit(static_cast<int>(_state)<static_cast<int>(arg._state)?_state:arg._state); //return minimum
}

Trit Trit::operator&& (const Trit& arg) const
{
return *this & arg;
}

/*Trit operator&& (bool arg) const
{
return arg?Trit(_state):Trit(T);
}
*/
Trit Trit::operator| (const Trit& arg) const
{
return Trit(static_cast<int>(_state)>static_cast<int>(arg._state)?_state:arg._state); //return minimum
}

Trit Trit::operator|| (const Trit& arg) const
{
return *this | arg;
}
/*
Trit operator|| (bool arg) const
{
return (!arg)?Trit(_state):Trit(T);
}
*/
bool trl::Trit::operator==(const Trit& arg) const
{
return _state==arg._state;
}

bool trl::Trit::operator!=(const Trit& arg) const
{
return _state!=arg._state;
}

bool trl::Trit::operator<(const Trit& arg) const
{
return static_cast<int>(_state)< static_cast<int>(arg._state);
}

bool trl::Trit::operator>(const Trit& arg) const
{
return static_cast<int>(_state) > static_cast<int>(arg._state);
}

Trit trl::Trit::operator^(const Trit& arg) const
{
return Trit(xorTable[static_cast<int>(_state)+1][static_cast<int>(arg._state)+1]);
}

Trit trl::Trit::operator*(const Trit& arg) const //Klini conjnuction
{
return Trit(kliniConjunctionTable[static_cast<int>(_state)+1][static_cast<int>(arg._state)+1]);
}

Trit trl::Trit::operator !() const
{/*
TritState tmp;
switch (_state)
{
	case TritState::T :tmp=TritState::I; break;
	case TritState::I :tmp=TritState::T; break;
	default: tmp=TritState::O;
	}*/
return Trit(static_cast<TritState>(-static_cast<int>(_state)));

}


Trit trl::Trit::operator -() const
{
return !(*this);
}
/*
operator int()
{
return static_cast<int>(_state);
}
*/
trl::Trit::operator char()
{
switch(_state)
{
	case TritState::T: return 'T';
	case TritState::O: return 'O';
	case TritState::I: return 'I';
}
}

trl::Trit::operator int() const
{
return static_cast<int>(_state);
}

Trit& trl::Trit::operator++()
{
_state=static_cast<TritState>((static_cast<int>(_state)+2)%3-1);
return *this;
}

Trit& trl::Trit::operator--()
{
_state=static_cast<TritState>((static_cast<int>(_state)-2)%3+1);
return *this;
}


const TritState Trit::xorTable[3][3]={{TritState::I,TritState::T,TritState::O},
				      {TritState::T,TritState::O,TritState::I},
				      {TritState::O,TritState::I,TritState::T}};
				      
const TritState Trit::kliniConjunctionTable[3][3]={{TritState::I,TritState::O,TritState::T},
				                   {TritState::O,TritState::O,TritState::O},
				                   {TritState::T,TritState::O,TritState::I}};

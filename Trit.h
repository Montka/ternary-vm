#ifndef TRIT
#define TRIT
#include <stdexcept>
namespace trl
{
enum class TritState
{
T=-1,
O=0,
I=1,
E=2
};

class Trit
{
public:
Trit();
Trit(bool);
Trit(TritState);
Trit (char);
Trit operator& (const Trit&) const;
Trit operator&& (const Trit&) const;
Trit operator| (const Trit&) const;
Trit operator|| (const Trit&) const;
bool operator==(const Trit&) const;
bool operator!=(const Trit&) const;
bool operator<(const Trit&) const;
bool operator>(const Trit&) const;
Trit operator^(const Trit&) const;
Trit operator*(const Trit&) const ;//Klini conjnuction
Trit operator !() const;
Trit operator -() const;
operator char();
explicit operator int() const;
Trit& operator++();
Trit& operator--();
private:
TritState _state;
static const TritState xorTable[3][3];
static const TritState kliniConjunctionTable[3][3];
};
}
#endif

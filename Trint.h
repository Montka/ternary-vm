#ifndef TRINT
#define TRINT
#include <iostream> 
#include <stdexcept>
#include <string>
#include "Trit.h"

namespace trl{
class Trint
{
public:
Trint();
Trint(std::string);
Trint(int);
Trint(const Trint&);
Trint& operator=(const Trint&);
Trint operator+(const Trint&) const;
Trint operator-() const;
Trint operator- (const Trint&) const;
Trint operator* (const Trint&) const;
Trint operator/ (const Trint&) const;
Trint& operator++();
Trint& operator--();
Trint& operator<<(int);
Trint& operator>>(int);
bool operator<(const Trint&) const;
bool operator<=(const Trint&) const;
bool operator>(const Trint&) const;
bool operator>=(const Trint&) const;
bool operator==(const Trint&) const;
bool operator!=(const Trint&) const;
Trint abs() const;
void print();
operator std::string();
private:
static const int trintLength=27;
Trit trits[trintLength];
Trit overflow(const Trit&, const Trit&, const Trit&) const;
void nullify();
int firstSignificantTrit() const;
int min(int, int) const;
};
}
#endif

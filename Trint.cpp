#include"Trint.h"
using namespace trl;
Trint::Trint()
{
nullify();
}
Trint::Trint(std::string str)
{

if(str.length() >27) throw std::runtime_error("Invalid value");

for(int i=1; i<=str.length(); ++i)
trits[trintLength-i]=str[str.length()-i];
}

Trint::Trint(int arg)
{
int tritPos=trintLength-1;
bool transferFlag=false;
bool negateFlag=false;
if(arg<0){negateFlag=true; arg*=-1;}
do
{
int residue=arg%3;
arg/=3;
if (transferFlag) {++trits[tritPos]; transferFlag=false;};
switch(residue)
{
case 0: break;
case 1: {transferFlag=trits[tritPos]==TritState::I?true:false;++trits[tritPos];break;}
case 2: {--trits[tritPos]; transferFlag=true; break;}
}
--tritPos;

}while (arg !=0);
if(transferFlag) ++trits[tritPos];
if (negateFlag) 
for(int i=firstSignificantTrit(); i<trintLength; ++i) trits[i]= !trits[i];

}

Trint::Trint(const Trint& arg)
{
for(int i=0; i<trintLength; ++i)
trits[i]=arg.trits[i];
}

Trint& Trint::operator=(const Trint& arg)
{
for(int i=0; i<trintLength; ++i)
trits[i]=arg.trits[i];
}
Trint Trint::operator+(const Trint& arg) const
{
Trint ret;
Trit overflowTrit, tmpTrit;

for (int i=trintLength-1; i>=min(firstSignificantTrit(), arg.firstSignificantTrit());--i)
{
ret.trits[i]=trits[i]^arg.trits[i]^overflowTrit;
overflowTrit=overflow(trits[i], arg.trits[i],overflowTrit);

}
ret.trits[ret.firstSignificantTrit()-1]=overflowTrit;
//if (overflowTrit != TritState::O) throw std::runtime_error("Addition overflow");
return ret;
}

Trint Trint::operator-() const 
{
Trint ret;
for(int i=firstSignificantTrit(); i<trintLength; ++i) ret.trits[i]=!trits[i];
return ret;
}

Trint Trint::operator- (const Trint& arg) const
{
return *this + (-arg);
}

Trint Trint::operator* (const Trint& arg) const
{
Trint ret;
Trint tmp;
for(int i=0; i<trintLength-firstSignificantTrit(); ++i)
{
for(int j=trintLength-1; j>=arg.firstSignificantTrit(); --j)
tmp.trits[j]=trits[trintLength-i-1] * arg.trits[j];
tmp << i;
//tmp.print();
//std::cout<<std::endl;
ret=ret+tmp;

tmp.nullify();
}
return ret;
}

Trint Trint::operator/ (const Trint& arg) const
{
if(arg == 0) throw std::runtime_error("Division by zero");
Trint quotinent;
Trint divided=*this;
Trint tmp=*this;
int shift=arg.firstSignificantTrit();
while(divided.abs()>=arg.abs() || shift <27)
//for(int k=0; k<3; ++k)
{

//std::cout<<std::endl<<"loop "<<std::endl;
//shift=0;

tmp=divided;
tmp>>(trintLength-shift);


while(tmp.abs() < arg.abs() && shift<27)
{
quotinent<<1;
++shift;

tmp=divided;
tmp>>(trintLength-shift);
}

//std::cout<<shift<<std::endl;
//tmp.print();
//std::cout<<std::endl;

Trint tmp_2=arg;
tmp_2<<(trintLength-shift);
//tmp_2.print();

//divided = divided - tmp_2;
if(tmp >0 ^ arg>0) {--quotinent;divided = divided + tmp_2;}
else {++quotinent;divided = divided - tmp_2;}

//std::cout<<std::endl;
//divided.print();
//std::cout<<std::endl;
//quotinent.print();

}
quotinent<<trintLength-shift;

return quotinent;

}

Trint& Trint::operator++()
{
*this=*this+1;
return *this;
}
Trint& Trint::operator--()
{
*this=*this-1;
return *this;
}

Trint& Trint::operator<<(int val)
{
Trit tmpArray[trintLength];
int startPoint;
int shift;
if (val == 0) return *this;
startPoint=firstSignificantTrit();
shift=startPoint-val;
if (shift<0) startPoint-=shift;
for(int i=startPoint; i<trintLength; ++i)
tmpArray[i-val]=trits[i];

for(int i=0; i<trintLength; ++i)
trits[i]=tmpArray[i];
return *this;
}

Trint& Trint::operator>>(int val)
{
Trit tmpArray[trintLength];
int StartPoint=trintLength-val-1;
if(StartPoint<firstSignificantTrit()) {nullify(); return *this;}

for(int i=StartPoint; i>=0; --i)
tmpArray[i+val]=trits[i];

for(int i=0; i<trintLength; ++i)
trits[i]=tmpArray[i];
return *this;
}


bool Trint::operator<(const Trint& arg) const
{
for(int i=0; i<trintLength; ++i) if(trits[i]!=arg.trits[i]) return trits[i]<arg.trits[i];
return false;
}

bool Trint::operator<=(const Trint& arg) const
{
for(int i=0; i<trintLength; ++i) if(trits[i]!=arg.trits[i]) return trits[i]<arg.trits[i];
return true;
}

bool Trint::operator>(const Trint& arg) const
{
for(int i=0; i<trintLength; ++i) if(trits[i]!=arg.trits[i]) return trits[i]>arg.trits[i];
return false;
} 

bool Trint::operator>=(const Trint& arg) const
{
for(int i=0; i<trintLength; ++i) if(trits[i]!=arg.trits[i]) return trits[i]>arg.trits[i];
return true;
} 

bool Trint::operator==(const Trint& arg) const
{
for(int i=0; i<trintLength; ++i) if(trits[i]!=arg.trits[i]) return false;
return true;
}

bool Trint::operator!=(const Trint& arg) const
{
return !(*this == arg);
}

Trint Trint::abs() const
{
return *this<0? -(*this) : *this;
}

void Trint::print()
{
for(int i=firstSignificantTrit(); i<trintLength; ++i) std::cout<<trits[i];
}

Trint::operator std::string()
{
std::string ret;
for(int i=0; i< trintLength; ++i)
ret+=trits[i];
return ret;
}

Trit Trint::overflow(const Trit& a, const Trit& b, const Trit& c) const
{
const TritState overflowTable[3][3][3]=	{{      {TritState::T, TritState::T, TritState::O},
						{TritState::T, TritState::O, TritState::O},
					        {TritState::O, TritState::O, TritState::O}},
					      
					       {{TritState::T, TritState::O, TritState::O}, //a=0
					        {TritState::O, TritState::O, TritState::O},
					        {TritState::O, TritState::O, TritState::I}},
					        
					       {{TritState::O, TritState::O, TritState::O},
					        {TritState::O, TritState::O, TritState::I},
					        {TritState::O, TritState::I, TritState::I}}};
return overflowTable[static_cast<int>(a)+1][static_cast<int>(b)+1][static_cast<int>(c)+1];
}

void Trint::nullify()
{
for(int i=0; i<trintLength; ++i)
trits[i]=TritState::O;
}

int Trint::firstSignificantTrit() const
{
for(int i=0; i<trintLength; ++i) if(trits[i]!=TritState::O) return i;
return trintLength-1;
}

int Trint::min(int a, int b) const
{
return a<b?a:b;
}


